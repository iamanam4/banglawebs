//import "./schema.js";
import {Mongo} from "meteor/mongo";
import { SimpleSchema } from "meteor/aldeed:simple-schema";

import "./schema/schema";
import "./schema/orderScheme";
import "./schema/tutorial";
import"./schema/contactSchema";
